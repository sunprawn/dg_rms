<?php

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');
    Route::post('refresh', 'Auth\LoginController@refresh');
    Route::get('user', function (Request $request) {
        return $request->user();
    });

    Route::apiResource('order', 'Api\OrderController')
        ->only(['index', 'store', 'show', 'update']);

    Route::apiResource('courier', 'Api\CourierController')
        ->only(['index', 'store', 'show', 'update']);

    Route::apiResource('import', 'Api\ImportController')
        ->only(['index', 'store', 'show', 'update']);

    Route::apiResource('retailer', 'Api\RetailerController')
        ->only(['index', 'store']);

    Route::apiResource('brand', 'Api\BrandController')
        ->only(['index', 'store']);

    Route::get('customer/{customer}', function (App\Models\Customer $customer) {
        return new Resource($customer);
    });
    Route::get('customer', 'Api\CustomerController@index');

    Route::get('product', 'Api\ProductController@index');
    Route::get('product/{id}', 'Api\ProductController@show')->where(['id' => '[1-9][0-9]{0,3}']);
    Route::get('product/{id}/items', 'Api\ProductController@items');
    Route::get('product/{barcode}', 'Api\ProductController@barcode');

});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');

    // disable register
    // Route::post('register', 'Auth\RegisterController@register');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
});
