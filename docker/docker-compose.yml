version: '3'

networks:
  frontend:
    driver: ${NETWORKS_DRIVER}
  backend:
    driver: ${NETWORKS_DRIVER}

volumes:
  memcached:
    driver: ${VOLUMES_DRIVER}

services:
  workspace:
    build:
      context: ./workspace
      args:
        - PHP_VERSION=${PHP_VERSION}
        - INSTALL_XDEBUG=${WORKSPACE_INSTALL_XDEBUG}
        - INSTALL_SOAP=${WORKSPACE_INSTALL_SOAP}
        - INSTALL_LDAP=${WORKSPACE_INSTALL_LDAP}
        - INSTALL_IMAP=${WORKSPACE_INSTALL_IMAP}
        - INSTALL_PHPREDIS=${WORKSPACE_INSTALL_PHPREDIS}
        - INSTALL_NODE=${WORKSPACE_INSTALL_NODE}
        - COMPOSER_GLOBAL_INSTALL=${WORKSPACE_COMPOSER_GLOBAL_INSTALL}
        - INSTALL_WORKSPACE_SSH=${WORKSPACE_INSTALL_WORKSPACE_SSH}
        - INSTALL_DEPLOYER=${WORKSPACE_INSTALL_DEPLOYER}
        - INSTALL_PYTHON=${WORKSPACE_INSTALL_PYTHON}
        - INSTALL_IMAGE_OPTIMIZERS=${WORKSPACE_INSTALL_IMAGE_OPTIMIZERS}
        - INSTALL_IMAGEMAGICK=${WORKSPACE_INSTALL_IMAGEMAGICK}
        - INSTALL_SWOOLE=${WORKSPACE_INSTALL_SWOOLE}
        - PUID=${WORKSPACE_PUID}
        - PGID=${WORKSPACE_PGID}
        - NODE_VERSION=${WORKSPACE_NODE_VERSION}
        - TZ=${WORKSPACE_TIMEZONE}
        - INSTALL_DEV=${WORKSPACE_INSTALL_DEV}
    volumes:
      - ${APP_PATH_HOST}:${APP_PATH_CONTAINER}
    extra_hosts:
      - "dockerhost:${DOCKER_HOST_IP}"
    ports:
      - "${WORKSPACE_SSH_PORT}:22"
    tty: true
    networks:
      - frontend
      - backend

### PHP-FPM ##############################################
  php-fpm:
    build:
      context: ./php-fpm
      args:
        - PHP_VERSION=${PHP_VERSION}
        - INSTALL_XDEBUG=${PHP_FPM_INSTALL_XDEBUG}
        - INSTALL_SOAP=${PHP_FPM_INSTALL_SOAP}
        - INSTALL_IMAP=${PHP_FPM_INSTALL_IMAP}
        - INSTALL_ZIP_ARCHIVE=${PHP_FPM_INSTALL_ZIP_ARCHIVE}
        - INSTALL_PHPREDIS=${PHP_FPM_INSTALL_PHPREDIS}
        - INSTALL_MEMCACHED=${PHP_FPM_INSTALL_MEMCACHED}
        - INSTALL_OPCACHE=${PHP_FPM_INSTALL_OPCACHE}
        - INSTALL_EXIF=${PHP_FPM_INSTALL_EXIF}
        - INSTALL_MYSQLI=${PHP_FPM_INSTALL_MYSQLI}
        - INSTALL_TOKENIZER=${PHP_FPM_INSTALL_TOKENIZER}
        - INSTALL_INTL=${PHP_FPM_INSTALL_INTL}
        - INSTALL_LDAP=${PHP_FPM_INSTALL_LDAP}
        - INSTALL_SWOOLE=${PHP_FPM_INSTALL_SWOOLE}
        - INSTALL_IMAGE_OPTIMIZERS=${PHP_FPM_INSTALL_IMAGE_OPTIMIZERS}
        - INSTALL_IMAGEMAGICK=${PHP_FPM_INSTALL_IMAGEMAGICK}
        - PUID=${WORKSPACE_PUID}
        - PGID=${WORKSPACE_PGID}
    volumes:
      - ./php-fpm/php${PHP_VERSION}.ini:/etc/php/php.ini
      - ${PHP_FPM_LOG_PATH}:/var/log/php
      - ${APP_PATH_HOST}:${APP_PATH_CONTAINER}
    expose:
      - "9000"
    extra_hosts:
      - "dockerhost:${DOCKER_HOST_IP}"
    environment:
      - PHP_IDE_CONFIG=${PHP_IDE_CONFIG}
    depends_on:
      - workspace
    networks:
      - backend

### NGINX Server #########################################
  nginx:
    build:
      context: ./nginx
      args:
        - PHP_UPSTREAM_CONTAINER=${NGINX_PHP_UPSTREAM_CONTAINER}
        - PHP_UPSTREAM_PORT=${NGINX_PHP_UPSTREAM_PORT}
        - PUID=${WORKSPACE_PUID}
        - PGID=${WORKSPACE_PGID}
    volumes:
      - ./nginx/nginx.conf:/etc/nginx/nginx.conf
      - ${APP_PATH_HOST}:${APP_PATH_CONTAINER}
      - ${NGINX_HOST_LOG_PATH}:/var/log/nginx
      - ${NGINX_SITES_PATH}:/etc/nginx/sites-available
    ports:
      - "${NGINX_HOST_HTTP_PORT}:80"
      - "${NGINX_HOST_HTTPS_PORT}:443"
    depends_on:
      - php-fpm
    networks:
      - frontend
      - backend

### Memcached ############################################
  memcached:
    build: ./memcached
    volumes:
      - ${DATA_PATH_HOST}/memcached:/var/lib/memcached
    ports:
      - "${MEMCACHED_HOST_PORT}:11211"
    depends_on:
      - php-fpm
    networks:
      - backend