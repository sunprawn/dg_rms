#! /bin/bash

WORK_DIR=/var/www

if [ "$INSTALL_DEV" = "true" ]; then
    COMPOSER_CMD="composer install --no-interaction -o"
else
    COMPOSER_CMD="composer install --no-dev --no-interaction -o"
fi

if [ ! -d "$WORK_DIR/vendor" ]; then
    echo 'Installing php dependencies'
    cd $WORK_DIR
    `$COMPOSER_CMD`
fi