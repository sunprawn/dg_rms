<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Courier;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class CourierController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $couriers = Courier::orderBy('sort')->get(['id', 'name', 'website', 'url']);
        return Resource::collection($couriers);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('couriers.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        try{
            $courier = Courier::firstOrNew([
                'name'      => $request->input('name'),
                'website'   => $request->input('website'),
                'url'       => $request->input('url'),
            ]);

            $courier->sort = $request->input('sort');

            $courier->save();

            return response()->json(['id' => $courier->id]);

        } catch (\Exception $e) {
            report($e);
            return response()->json(['message' => $e], 500);
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
