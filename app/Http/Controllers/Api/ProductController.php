<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ImportItem;
use App\Http\Resources\ProductCollection;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Resources\Json\Resource;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $products = Product::orderBy('stock', 'desc')
            ->with(['brand' => function ($query) {
                $query->select('id', 'name');
            }])
            ->paginate(Input::get('size'));

        return Resource::collection($products);
    }

    public function show($id)
    {
        try {
            $product = Product::where('id', $id)
                ->with(['brand' => function($query) {
                    $query->select('id', 'name');
                }])
                ->firstOrFail(['id', 'barcode', 'name', 'size', 'brand_id', 'stock']);

            return new Resource($product);

        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['message' => 'Product not found'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage() ], 500);
        }

    }

    public function items($id)
    {
        try {

            $items = ImportItem::where('product_id', $id)
                ->orderBy('import_id', 'desc')
                ->with(['import' => function($query) {
                    $query->select('id', 'retailer_id', 'purchase_date')
                        ->with(['retailer' => function($query) {
                            $query->select('id', 'name');
                        }]);
                }])
                ->paginate(80);

            return Resource::collection($items);

        } catch (\Exception $e) {
            return response()->json(['message' => 'Items not found'], 500);
        }

    }

    /**
     * Display the product by barcode
     *
     * @param $barcode
     */
    public function barcode($barcode) {
        $product = Product::where('barcode', $barcode)
            ->with(['brand' => function($query) {
                $query->select('id', 'name');
            }])
            ->first(['name', 'size', 'brand_id', 'stock']);

        if (!$product) {
            return response()->json(['message' => 'Product not found'], 404);
        }
        return new Resource($product->noAppends());
    }
}
