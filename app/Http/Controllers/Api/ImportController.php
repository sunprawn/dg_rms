<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Import;
use App\Models\ImportItem;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Retailer;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\DB;

class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = ImportItem::select('import_id', 'product_id', 'quantity', 'left', 'price')
            ->orderBy('import_id', 'desc')
            ->with(['product' => function ($query) {
                $query->select('id', 'barcode', 'name', 'brand_id')
                    ->with(['brand' => function ($query) {
                        $query->select('id', 'name');
                    }]);
            }])
            ->paginate(Input::get('size'));

        return Resource::collection($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $retailer = Retailer::findOrFail($request->json('retailer'));

            $import = new Import();
            $import->retailer()->associate($retailer);
            $import->purchase_date = $request->json('purchased_date');

            /*
             * save the import with related retailer
             */
            //$retailer->imports->save($import);
            $import->save();

            foreach($request->json('items') as $item) {
                $product = Product::where('barcode', $item['barcode'])->first();

                /*
                 * if provided brand is id, find it
                 * otherwise create a new Brand
                 */
                if (is_numeric($item['brand'])) {
                    $brand = Brand::find($item['brand']);
                } else {
                    $brand = Brand::create([
                        'name' => $item['brand'],
                    ]);
                }


                /*
                 * if product not exist, create the product
                 */
                if($product == null) {

                    $product = new Product();
                    $product->barcode = $item['barcode'];
                    $product->name = $item['name'];
                    $product->size = $item['size'];

                    $product->brand()->associate($brand);
                    $product->save();
                }

                $importItem = new ImportItem();
                $importItem->quantity = $item['quantity'];
                $importItem->left = $item['quantity'];
                $importItem->price = $item['price'];

                $importItem->product()->associate($product);
                $importItem->import()->associate($import);
                $importItem->save();

                $product->stock += $item['quantity'];
                $product->save();
            }
            //throw new Exception("wrong");
            DB::commit();

            $result = [
                'id'  => $import->id
            ];
            return response()->json($result);

        } catch (\Exception $e){

            DB::rollback();
            report($e);
            return response()->json(['message' => $e], 500);


        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $import = Import::with(['importItems' => function ($query) {
                $query->select('id', 'import_id', 'product_id', 'quantity', 'left', 'price')
                    ->with(['product' => function ($query) {
                        $query->select('id', 'barcode', 'name', 'brand_id', 'size')->with('brand');
                    }]);
            }])->with('retailer')
            ->find($id);

        //var_dump($import->importItems);
        return new Resource($import);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }
}
