<?php

namespace App\Http\Controllers\Api;

use App\Models\Courier;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::select('id','tracking','shipping','cost','paid','profit','dispatch', 'received','customer_id','courier_id')
            ->orderBy('dispatch', 'desc')
            ->orderBy('id', 'desc')
            ->with(['customer' => function($query) {
                $query->select('id', 'name');
            }])
            ->with(['courier' => function($query) {
                $query->select('id', 'url');
            }])
            ->with('lastShippingLog')
            ->paginate(Input::get('size'));

        return Resource::collection($orders);
    }

    public function show($id)
    {
        $order = Order::with('customer')
            ->with('courier')
            ->with([
                'orderItems' => function ($query) {
                    $query->select('id', 'quantity', 'postage', 'cost', 'order_id', 'product_id'); //must have order_id, product_id
                },
                'orderItems.product' => function ($query) {
                    $query->select('id', 'barcode', 'name', 'brand_id');
                },
                'orderItems.product.brand' => function ($query) {
                    $query->select('id', 'name');
                }
            ])  // or ->with('orderItems.product.brand')
            ->with('shippingLogs')
            ->find($id);

        // load the related product
        return new Resource($order);
    }

    public function update($id)
    {
        try {
            $order = Order::findOrFail($id);

            $data = Input::all();

            /*foreach ($data as $key => $value ) {
                if ($key == 'id') continue;
                $order->$key = $value;
            }*/
            $order->update($data);

        } catch (\Exception $e) {

        }

    }

    public function store(Request $request)
    {

        $result = [];
        //$inputCustomer = $request->json('customer');

        try {
            DB::beginTransaction();

            /*
             * get the customer or create new
             */
            $customer = Customer::firstOrCreate([
                'name'      => $request->json('name'),
                'phone'     => $request->json('phone'),
                'address'   => $request->json('address'),
                'identity'  => $request->json('identity')
            ]);

            /*
             * get the courier
             */
            $courier = Courier::findOrFail($request->json('courier'));

            /*
             * calculate paid amount
             */
            $paidAmount = 0;
            if (!$request->json('paid')) {
                foreach($request->json('items') as $item) {
                    $paidAmount += $item['quantity'] * $item['price'] + $item['postage'];
                }
            } else {
                $paidAmount = $request->json('paid');
            }

            /*
             * Create new order
             */
            $order = new Order();
            /*$order = Order::create([
                'tracking'  => $request->json('tracking'),
                'shipping'  => $request->json('shipping'),
                'paid'      => $paidAmount,
                'dispatch'  => $request->json('dispatch')
            ]);*/
            $order->tracking  = $request->json('tracking');
            $order->shipping  = $request->json('shipping');
            $order->paid      = $paidAmount;
            $order->dispatch  = $request->json('dispatch');
            $order->received  = false;
            $order->courier()->associate($courier);
            $order->customer()->associate($customer);
            $order->save();

            /*
             *  Create order items
             */
            foreach($request->json('items') as $item) {
                $product = Product::where('barcode', $item['barcode'])->first();

                $orderItem = new OrderItem();
                $orderItem->product()->associate($product);
                $orderItem->order()->associate($order);
                $orderItem->quantity = $item['quantity'];
                $orderItem->price = $item['price'];
                $orderItem->postage = $item['postage'];
                $orderItem->save();
                $orderItem->calculateImportItems();

                $order->cost += $orderItem->cost;
            }

            $order->profit = $order->paid - ($order->cost + $order->shipping) * (session('rate')?session('rate'):6);
            $order->save();

            $result = [
                'orderID'   => $order->id,
            ];

            DB::commit();
            return response()->json($result);
        } catch (\Exception $e) {

            // debug message
            //$result['error'] = $e->getMessage();
            DB::rollback();

            return response()->json(['message' => 'Cannot create order'], 500);
        }


    }
}
