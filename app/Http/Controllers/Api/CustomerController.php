<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Http\Resources\Json\Resource;

class CustomerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$customers = Customer::select('id', 'name', 'phone','address','identity')
            ->paginate(80);

        return Resource::collection($customers);
	}

    public function get($name, $identity)
    {
        $customer = Customer::where('name', $name)
            ->where('identity', $identity)
            ->orderBy('id', 'desc')
            ->first();

        if($customer){
            $result = [
                'error'     => '',
                'address'   => $customer->address,
                'phone'     => $customer->phone
            ];
        } else {
            $result = [
                'error' => 'not found'
            ];
        }

        return response()->json($result);
    }

}
