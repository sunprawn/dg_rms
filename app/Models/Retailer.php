<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 9/04/15
 * Time: 10:06 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Retailer extends Model {

    protected $fillable = ['name', 'sort'];
    public $timestamps = false;

    public function imports()
    {
        return $this->hasMany('App\Models\Import');
    }

    public function importItems()
    {
        return $this->hasManyThrough('App\Models\ImportItem', 'App\Models\Import');
    }

}