<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\OrderItem;

class Product extends Model {

    protected $fillable = ['barcode', 'name', 'size', 'stock'];
    protected $appends = ['sold'];
    protected $hidden = ['created_at', 'updated_at'];

    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }

    public function importItems()
    {
        return $this->hasMany('App\Models\ImportItem');
    }

    public function orderItems()
    {
        return $this->hasMany('App\Models\OrderItem');
    }

    public function getRouteKeyName()
    {
        return 'barcode';
    }

    public function getSoldAttribute()
    {
        return (Int) OrderItem::byPID($this->id)->sum('quantity');
    }

    // remove the appends
    public function noAppends()
    {
        $this->appends = [];
        return $this;
    }

    /*public function getQuantityAttribute()
    {
        $quantity = 0;

        $import_item = ImportItem::byPID($this->id)
                        ->where('left', '>', 0)->get();

        foreach($import_item as $item) {
            $quantity += $item->left;
        }

        return $quantity;
    }*/

}