<?php
/**
 * Created by PhpStorm.
 * User: simony
 * Date: 26/04/15
 * Time: 5:23 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingLog extends Model{

    protected $fillable = ['order_id', 'time', 'location', 'event'];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}