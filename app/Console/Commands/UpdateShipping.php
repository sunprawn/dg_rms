<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;
use App\Models\ShippingLog;

class UpdateShipping extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shipping:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the shipping information for each order';

    private $delivered_arr = ['签收', '妥投'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $xpathQuery = [
            1 => '//div[@id="oDetail"]/table//tr[last()]/td',   // ARK express
            2 => '//div[contains(concat(" ", @class, " "), "track-right")]/table/tbody//tr[last()]/td',   // EWE express
        ];

        $matchingTable = [
            1 => [1, 2],                // same pattern
        ];

        $orders = Order::where('received', '!=', 1)->get();

        foreach($orders as $order) {

            $this->info("[ " . date('Y-m-d H:i:s') . " ] processing:  " . $order->tracking);

            /*
             * get result page with different courier
             */
            try {
                $url = $order->courier->url . $order->tracking;
                $result = $this->getContentFromGet($url);

            } catch (\Exception $e) {
                $this->error($e->getMessage());
                continue;
            }


            /*
             * if no result or no match xpath query
             */
            if (!$result || !isset($xpathQuery[$order->courier->id])) {
                continue;
            }

            //$result = mb_convert_encoding(file_get_contents($url), "utf-8", 'gb2312');

            /*
             * if the courier is ARK
             */
            if ($order->courier->id == 1) {
                $dom = new \DOMDocument();
                @$dom->loadHTML($result);
                $xpath = new \DOMXPath($dom);

                $data = $xpath->query($xpathQuery[$order->courier->id]);

                /*
                 * For 方舟速递
                 */
                if($data->length == 3 && in_array($order->courier->id, $matchingTable[1])) {
                    $shipping = ShippingLog::firstOrNew([
                        'order_id'  => $order->id,
                        'time'      => $data->item(0)->nodeValue,
                        'location'  => $data->item(1)->nodeValue,
                        'event'     => $data->item(2)->nodeValue,
                    ]);

                    $shipping->save();
                }

            } elseif ($order->courier->id == 2) {
                $result = json_decode($result, true);
                $details = $result['Payload'][0]['Details'];
                $recode = array_pop($details);

                if (count($recode) >= 3) {
                    $shipping = ShippingLog::firstOrNew([
                        'order_id'  => $order->id,
                        'time'      => $recode['DateString'],
                        'location'  => $recode['Place'],
                        'event'     => $recode['Message'],
                    ]);

                    $shipping->save();
                }
            }

            foreach ($this->delivered_arr as $devStr) {
                if (strpos($shipping->event, $devStr) !== false) {

                    $order->received = true;
                    $order->save();
                    break;
                }
            }


            if (!app()->environment('local')) {
                // The environment is local
                //$this->info($shipping->event);
                sleep(3);
            }


        }
    }

    protected function getContentFromPost( $url,  $count, $field_string) {
        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, $count);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $field_string);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        return $result;
    }

    protected function getContentFromGet($url) {
        return file_get_contents($url);
    }
}
