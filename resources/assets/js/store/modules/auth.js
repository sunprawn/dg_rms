import axios from 'axios'
import Cookies from 'js-cookie'

export const LOGOUT = 'LOGOUT'
export const SAVE_TOKEN = 'SAVE_TOKEN'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE'
export const UPDATE_USER = 'UPDATE_USER'

export default {
    state: {
        user: null,
        token: Cookies.get('token')
    },

    namespaced: true,

    getters: {
        user: state => state.user,
        token: state => state.token,
        check: state => state.user !== null
    },
    mutations: {
        [SAVE_TOKEN] (state, { token, remember }) {
            state.token = token
            Cookies.set('token', token, { expires: remember ? 365 : null })
        },

        [FETCH_USER_SUCCESS] (state, { user }) {
            state.user = user
        },

        [FETCH_USER_FAILURE] (state) {
            state.token = null
            Cookies.remove('token')
        },

        [LOGOUT] (state) {
            state.user = null
            state.token = null

            Cookies.remove('token')
        },

        [UPDATE_USER] (state, { user }) {
            state.user = user
        }
    },
    actions: {
        saveToken ({ commit, dispatch }, payload) {
            commit(SAVE_TOKEN, payload)
        },

        async fetchUser ({ commit }) {
            try {
                const { data } = await axios.get('/api/user')

                commit(FETCH_USER_SUCCESS, { user: data })
            } catch (e) {
                commit(FETCH_USER_FAILURE)
                throw e
            }
        },

        async refresh ({ commit }) {
            try {
                const { data } = await axios.post('/api/refresh')

                commit(SAVE_TOKEN, { token: data.token, remember: 1 })
            } catch (e) {
                commit(LOGOUT)
                throw e
            }
        },

        updateUser ({ commit }, payload) {
            commit(UPDATE_USER, payload)
        },

        async logout ({ commit }) {
            try {
                await axios.post('/api/logout')
            } catch (e) { }

            commit(LOGOUT)
        }
    }
}