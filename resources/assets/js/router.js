import Vue from 'vue';
import VueRouter from 'vue-router';

import store from './store';

import Login    from './pages/auth/login'
import Register from './pages/auth/register'

import Orders       from './pages/orders/order-list'
import OrderCreate  from './pages/orders/order-create'
import OrderShow    from './pages/orders/order-detail'

import Product      from './pages/product'
import ProductIndex from './pages/products/product-index'
import ProductDetail from './pages/products/product-detail'
import ImportList   from './pages/products/import-list'
import ImportDetail from './pages/products/import-detail'
import ImportCreate from './pages/products/import-create'

import Other        from './pages/other'
import BrandList    from './pages/others/brand-list'
import BrandCreate  from './pages/others/brand-create'
import CustomerList from './pages/others/customer-list'
import RetailerList from './pages/others/retailer-list'
import RetailerCreate  from './pages/others/retailer-create'
import CourierList   from './pages/others/courier-list'
import CourierCreate from './pages/others/courier-create'



Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',

    routes: [
        { path: '/', redirect: '/order' },
        { path: '/login', name: 'login', component: Login, meta: { auth: false } },
        // hide register
        // { path: '/register', name: 'register', component: register, meta: { auth: false } },
        {
            path: '/order',  component: { render (c) {return c('router-view')}},
            children: [
                { path: '', name: 'order', component: Orders},
                { path: 'create', name: 'order.create', component: OrderCreate },
                { path: ':id', name: 'order.show', component: OrderShow }
            ]
        },

        {
            path: '/other', component: Other,
            children: [
                { path: '', redirect: { name: 'other.customer' }},
                { path: 'customer', name: 'other.customer', component: CustomerList},
                { path: 'brand', name: 'other.brand', component: BrandList},
                { path: 'brand/create', name: 'other.brand.create', component: BrandCreate},
                { path: 'retailer', name: 'other.retailer', component: RetailerList},
                { path: 'retailer/create', name: 'other.retailer.create', component: RetailerCreate},
                { path: 'courier', name: 'other.courier', component: CourierList},
                { path: 'courier/create', name: 'other.courier.create', component: CourierCreate},
            ]
        },

        {
            path: '', component: Product,
            children: [
                {path: 'product', name: 'product', component: ProductIndex},
                {path: 'product/:id', name: 'product.detail', component: ProductDetail},
                {path: 'import', name: 'import', component: ImportList},
                {path: 'import/create', name: 'import.create', component: ImportCreate},
                {path: 'import/:id', name: 'import.detail', component: ImportDetail},
            ]
        },

        {
            path: '*',
            redirect: '/login'
        }
    ]
});

router.beforeEach(({meta, path}, from, next) => {
    var { auth = true } = meta

    // if not login on other pages
    if (auth && !store.getters['auth/token']) {
        return next({ path: '/login' })
    }

    // get user if authenticated
    if (!store.getters['auth/check'] && store.getters['auth/token']) {
        try {
            store.dispatch('auth/fetchUser')

            // refresh token after 2 seconds
            setTimeout(()=>(store.dispatch('auth/refresh')), 2000)
            //store.dispatch('auth/refresh')
        } catch (e) { return next({ path: '/login' }) }
    }
    next()
})

export default router